# K8 kubernetes

This class will be an introduction to Kubernetes K8. 

The problem:
- our muffin store is doing great
- we add more servers, and need to add a load balancer
- then we need more server and need a second load balances and a bunch more instances
- Ohh now we need to scaled down...
- and now we need to change the because we have new muffins!

If only we had a container orchestration tool!

We do: **Kubernetes!**

## Kubernetes

Let's understand how it works. 
First you have your main machines:

- Master x
- Worker nodes(x2 or more)
- pods (launching on worker node)



#### **Master**

you might have two of these in case on dies.
- naming
- networking
- placement (where did I launch that pod, where it it running)

For the worker nodes to have full internet access, its best o have a HA proxy pod. The IP of this HA proxy will the an entry point to the cluster. 
You'll then assign a Load balancer (ELB) to point to it and also above set a DNS with a will card point to it. 

and 

#### **WorkerNodes**

Machine running docker that lunches Pods

#### **POD**

Get launched on worker nodes. Podes are:
- configuration of docker containers
- Health check 
- how many containers and other atribures 


**name space** in k8 is kind like a VPC

**service** is like a proxy / load balancer

etcd - is service discovery. Basically a DB, key:pair that keeps that of what got lunched and where


## Installing K8

Best way to start a cluster is by using ansible kubespray.

You need ansible and 3 host machines.

You'll specify which one will be the master and the worker nodes. 

## Main way of communicating 

- kubectl
- (you might look at) helm

## If you don't want to lock your self to cloud provider - you build your own cluster. 


## Making our own cluster minimum requierments

- minimum 16ram 
- 3ta.xlarge x3 with 30gb (k8master + 2 k8workerss)
- t2.micro with 20gb (k8bastion - to kubebspray and ansible)


### Install using kubspray

We are going to install using kube spray:

- https://github.com/kubernetes-sigs/kubespray#requirements

- Start machines
  - Done
- Allow all traffic from subnet into all machines
  - Done
- exchnage keys 
  - generate a key on the ansible / bastion machine
  - `ssh-keygen`
  - go to k8 nodes and add to authorizedkeys 
  - Try to ssh
  - Done
- Install requierement from offical documentation 
  - we need:
    - git `sudo yum -y install git`
    - python3 (happy days)
  - clone the kubespray project
  - install the requierements.txt 
  - Done

- expoort AWS_DEFAULT_PROFILE=Academy
  
- Change your inventory file

- Run Ansible playbooks to kub spray 
- `ansible-playbook -i inventory/alk8cluster/inventory.ini --become --become-user=root cluster.yml`
- `ansible-playbook -i inventory/mycluster/inventory.ini --become --become-user=root cluster.yml`


After inside the Master Node run:

`sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf`

### Main kubectl Commands

```
$ kubectl get namespace
$ sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
$ kubectl create ns <name>

$ touch nginx-app.yaml 
# https://kubernetes.io/docs/concepts/cluster-administration/manage-deployment/

# https://www.bmc.com/blogs/kubernetes-services/

$ kubectl apply -f nginx-app.yaml -n <namespace>
$ kubectl get deploy -n <namespace>
$ kubectl describe deploy -n <namespace> 

$ kubectl get pods -n <namespace> -checks what pods are live
$ kubectl delete pod <name of pod -n <namespace>
$ kubectl get svc -n <namespace>
$ curl <cluster ips>
$ kubectl delete svc filipe-loadbalancer-svc -n webns 
```